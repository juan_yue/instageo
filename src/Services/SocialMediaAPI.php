<?php

namespace Services;

/**
 * Interface to define the services of a Social Media API.
 **/
interface SocialMediaAPI
{
    /**
     * Returns the login URL.
     *
     * @return string The login URL
     **/
    public function getLoginURL();

    /**
     * Returns the most recent media published by the owner of the access_token.
     *
     * @param int    $n         the number of thumb images to return
     * @param string $authToken the authorization token
     *
     * @return array An array of ThumbImages
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    public function getRecentMedia($n, $authToken);

    /**
     * Returns information about a media object.
     *
     * @param string $mediaId   the media identifier
     * @param string $authToken the authorization token
     *
     * @return MediaLocation A Media Location
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    public function getMedia($mediaId, $authToken);

    /**
     * Generates the Social Media OAUTH token.
     *
     * @param string $code the Social Media authorization code
     *
     * @return UserDetails the user details
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    public function generateToken($code);
}
