<?php

namespace Services;

use Config\ConfigLoader;
use Exceptions\ServiceApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Models\GeoPoint;
use Models\Location;
use Models\MediaLocation;
use Models\ThumbImage;
use Models\UserDetails;

/**
 * Class to define the requests and perform the calls to the Instagram API.
 **/
class InstagramAPI implements SocialMediaAPI
{
    private $igUrl;
    private $igOAuthURL;

    private $igClientID;
    private $igClientSecretID;
    private $igRedirectURI;

    private $httpClient;

    /**
     * InstagramAPI constructor.
     *
     * @param ConfigLoader $config     the configuration properties
     * @param Client       $httpClient the http client
     **/
    public function __construct(ConfigLoader $config, $httpClient = null)
    {
        $this->igUrl = $config->getApiUrl('instagram');

        $this->igClientID = $config->getClientId('instagram');
        $this->igClientSecretID = $config->getClientSecretId('instagram');
        $this->igRedirectURI = $config->getRedirectUri('instagram');
        $this->igOAuthURL = $config->getOauthUrl('instagram');

        if (!$httpClient) {
            $this->httpClient = new Client();
        } else {
            $this->httpClient = $httpClient;
        }
    }

    /**
     * Returns the most recent media published in Instagram by the owner of the access_token.
     *
     * @param int    $n         the number of thumb images to return
     * @param string $authToken the authorization token
     *
     * @return array An array of ThumbImages
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    public function getRecentMedia($n, $authToken)
    {
        $response = $this->call('/users/self/media/recent/', $authToken);

        $array = [];

        if ($response != null) {
            if ($n > count($response->data)) {
                $n = count($response->data);
            }

            for ($i = 0; $i < $n + 0; ++$i) {
                $image = new ThumbImage($response->data[$i]->id, $response->data[$i]->images->thumbnail->url);
                array_push($array, $image);
            }
        }

        return $array;
    }

    /**
     * Calls to the Instagram API.
     *
     * @param string $path      the path to be attached to the service URL
     * @param string $authToken the authorization token
     *
     * @return string A json response
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    private function call($path, $authToken)
    {
        // If there's no token, the request couldn't be performed
        if (!$authToken) {
            throw new ServiceApiException('Authentication is required', 403);
        }

        try {
            $response = $this->httpClient->get($this->igUrl.$path.'?access_token='.$authToken, ['verify' => false]);
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody());
            if ($body && property_exists($body, 'meta') && property_exists($body->meta, 'error_message')) {
                $errorMessage = $body->meta->error_message;
            } else {
                $errorMessage = $e->getResponse()->getBody();
            }
            throw new ServiceApiException($errorMessage, $e->getCode());
        }

        return json_decode($response->getBody());
    }

    /**
     * Returns information about a media object in Instagram.
     *
     * @param string $mediaId   the media identifier
     * @param string $authToken the authorization token
     *
     * @return MediaLocation A Media Location
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    public function getMedia($mediaId, $authToken)
    {
        $response = $this->call('/media/'.$mediaId, $authToken);

        $location = null;

        if ($response != null) {
            $igLocation = $response->data->location;

            if ($igLocation != null) {
                $geoPoint = new GeoPoint($igLocation->longitude, $igLocation->latitude);
                $location = new Location($igLocation->id, $igLocation->name, $geoPoint);
            }
        }

        $mediaLocation = new MediaLocation($mediaId, $location);

        return $mediaLocation;
    }

    /**
     * Returns the Instagram login URL.
     *
     * @return string The login URL
     **/
    public function getLoginURL()
    {
        $url = $this->igOAuthURL.'/authorize';
        $data = ['client_id' => $this->igClientID, 'redirect_uri' => $this->igRedirectURI, 'response_type' => 'code'];

        return urldecode(sprintf('%s?%s', $url, http_build_query($data)));
    }

    /**
     * Generates the Instagram OAUTH token.
     *
     * @param string $code the Instagram authorization code
     *
     * @return UserDetails the user details
     *
     * @throws ServiceApiException (message, HTTP error code)
     **/
    public function generateToken($code)
    {
        $url = $this->igOAuthURL.'/access_token';

        $fields = [
            'client_id' => $this->igClientID,
            'client_secret' => $this->igClientSecretID,
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->igRedirectURI,
            'code' => $code,
        ];

        $body = ['form_params' => $fields, 'verify' => false];

        $response = null;

        try {
            $response = $this->httpClient->post($url, $body);
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody());
            if ($body && property_exists($body, 'meta') && property_exists($body->meta, 'error_message')) {
                $errorMessage = $body->meta->error_message;
            } else {
                $errorMessage = $e->getResponse()->getBody();
            }
            throw new ServiceApiException($errorMessage, $e->getCode());
        }

        $userDetails = null;

        if ($response != null) {
            $response = json_decode($response->getBody());
            $userDetails = new UserDetails(
                $response->user->id, $response->user->username,
                $response->access_token
            );
        }

        return $userDetails;
    }
}
