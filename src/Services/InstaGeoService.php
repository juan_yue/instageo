<?php

namespace Services;

use Exceptions\ServiceApiException;
use Models\ErrorResponseContent;
use Models\OKResponseContent;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class to define the REST API services.
 **/
class InstaGeoService
{
    private $socialMediaAPI;

    /**
     * InstaGeoService constructor.
     *
     * @param SocialMediaAPI $socialMediaAPI Social media API instance
     **/
    public function __construct(SocialMediaAPI $socialMediaAPI)
    {
        if (!headers_sent()) {
            session_start();
        }

        $this->socialMediaAPI = $socialMediaAPI;
    }

    /**
     * Returns the Social Media API login URL.
     *
     * @return string the Social Media API login URL
     **/
    public function getLoginUrl()
    {
        $loginUrl = '';
        if ($this->socialMediaAPI != null) {
            $loginUrl = $this->socialMediaAPI->getLoginURL();
        }

        return $loginUrl;
    }

    /**
     * Retrieves from the Social Media API the thumb images of the latest N media published by the owner of the access token.
     *
     * @param int    $n         the number of thumb images to return
     * @param string $authToken the auth token
     *
     * @return Response
     **/
    public function getLatestThumbImages($n, $authToken = null)
    {
        $latestMedia = null;

        try {
            if ($authToken == null) {
                $authToken = $this->getToken();
            }
            $latestMedia = $this->socialMediaAPI->getRecentMedia($n, $authToken);
        } catch (ServiceApiException $e) {
            $responseContent = (new ErrorResponseContent($e->getCode(), $e->getMessage()))->toJSON();

            return new Response($responseContent, $e->getCode());
        }

        $data = [
            'total' => count($latestMedia),
            'images' => $latestMedia,
        ];

        $responseContent = (new OKResponseContent($data))->toJSON();

        return new Response($responseContent);
    }

    /**
     * Returns the Social Media API OAUTH token saved in session.
     *
     * @return string $code the Social Media API OAUTH token
     **/
    private function getToken()
    {
        if (isset($_SESSION['instaGeo_userDetails']) &&
            property_exists($_SESSION['instaGeo_userDetails'], 'authToken')
        ) {
            return $_SESSION['instaGeo_userDetails']->authToken;
        }

        return null;
    }

    /**
     * Retrieves from the Social Media API information about the location of a given media.
     *
     * @param string $mediaId   the media id
     * @param string $authToken the auth token
     *
     * @return Response
     **/
    public function getMediaLocation($mediaId, $authToken = null)
    {
        $igLocation = null;

        try {
            if ($authToken == null) {
                $authToken = $this->getToken();
            }
            $igLocation = $this->socialMediaAPI->getMedia($mediaId, $authToken);
        } catch (ServiceApiException $e) {
            $responseContent = (new ErrorResponseContent($e->getCode(), $e->getMessage()))->toJSON();

            return new Response($responseContent, $e->getCode());
        }

        if (!$igLocation || !$igLocation->location) {
            $responseContent = (new ErrorResponseContent(404, 'Location not found in media'))->toJSON();

            return new Response($responseContent, 404);
        }

        $responseContent = (new OKResponseContent($igLocation))->toJSON();

        return new Response($responseContent);
    }

    /**
     * Requests the Social Media API access token and store it in session.
     *
     * @param string $code the exact code you received during the authorization step
     *
     * @return Response
     **/
    public function generateToken($code)
    {
        try {
            $response = $this->socialMediaAPI->generateToken($code);
            $_SESSION['instaGeo_userDetails'] = $response;

            return new Response((new OKResponseContent())->toJSON());
        } catch (ServiceApiException $e) {
            $responseCode = $e->getCode();
            $responseContent = (new ErrorResponseContent($e->getCode(), $e->getMessage()))->toJSON();
        }

        return new Response($responseContent, $responseCode);
    }
}
