<?php

namespace Controllers;

use Config\ConfigLoader;
use DI\ContainerBuilder;
use Services\InstagramAPI;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class to define the controllers related to the instaGeo API.
 */
class InstaGeoController
{
    private $restAPI;
    private $config;

    /**
     * InstaGeoController constructor.
     */
    public function __construct()
    {
        $this->config = ConfigLoader::getInstance();

        //define the service api container
        $builder = new ContainerBuilder();
        $container = $builder->build();
        $container->set('Services\SocialMediaAPI', new InstagramAPI($this->config));

        $this->restAPI = $container->get('Services\InstaGeoService');
    }

    /**
     * Home page.
     *
     * @param Application $app the application
     *
     * @return Render the page
     */
    public function home(Application $app)
    {
        $username = '';
        $loginURL = '';

        //if there's user information in session
        if (!empty($_SESSION['instaGeo_userDetails'])) {
            $username = $_SESSION['instaGeo_userDetails']->username;
        } else {
            $loginURL = $this->restAPI->getLoginUrl();
        }

        $data = [
            'loginUrl' => $loginURL,
            'username' => $username,
            'googleMapApiUrl' => $this->config->getApiUrl('google_map').$this->config->getApiKey('google_map'),
        ];

        return $app['twig']->render('index.html.twig', $data);
    }

    /**
     * Retrieves the latest N thumb images uploaded by the user.
     *
     * @param int     $n       the number of thumb images to return
     * @param Request $request the request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getLatestThumbImages($n, Request $request)
    {
        $authToken = $request->get('authToken');

        return $this->restAPI->getLatestThumbImages($n, $authToken);
    }

    /**
     * Retrieves information about location for a specific media.
     *
     * @param string  $mediaId the media id
     * @param Request $request the request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getMediaLocation($mediaId, Request $request)
    {
        $authToken = $request->get('authToken');

        return $this->restAPI->getMediaLocation($mediaId, $authToken);
    }

    /**
     * Application login.
     * Generates the auth token and redirects to the main page.
     * If there's an error with the token, a message will be displayed.
     *
     * @param Application $app     the application
     * @param Request     $request the request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function login(Application $app, Request $request)
    {
        $requestParams = $request->query->all();
        $response = null;

        if (array_key_exists('code', $requestParams)) {
            $response = $this->restAPI->generateToken($requestParams['code']);
        }

        if ($response != null && $response->getStatusCode() == 200) {
            return $app->redirect('/');
        }

        $errorMessage = 'Login fail';
        if ($response != null) {
            $errorMessage = json_decode($response->getContent())->meta->errorMessage;
        }

        $data = [
            'errorMessage' => $errorMessage,
            'loginUrl' => $this->restAPI->getLoginUrl(),
        ];

        return $app['twig']->render('index.html.twig', $data);
    }

    /**
     * Removes all the user information from session and redirects to the main page.
     *
     * @param Application $app the application
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Application $app)
    {
        unset($_SESSION['instaGeo_userDetails']);

        return $app->redirect('/');
    }
}
