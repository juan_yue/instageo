<?php

namespace Controllers;

use Symfony\Component\HttpFoundation\Response;

$app->get('/', 'instaGeo.controller:home')->bind('homepage');

$app->error(
    function ($code) use ($app) {
        if ($app['debug']) {
            return;
        }

        // 404.html, or 40x.html, or 4xx.html, or error.html
        $templates = [
            'errors/'.$code.'.html.twig',
            'errors/'.substr($code, 0, 2).'x.html.twig',
            'errors/'.substr($code, 0, 1).'xx.html.twig',
            'errors/default.html.twig',
        ];

        return new Response($app['twig']->resolveTemplate($templates)->render(['code' => $code]), $code);
    }
);

// REST API CONTROLLERS

/*
 * PATH: /api/images/{n}?authToken={authToken}
 * authToken parameter is optional.
 **/
$app->get('/api/images/{n}', 'instaGeo.controller:getLatestThumbImages');

/*
 * PATH: /api/media/{mediaId}?authToken={authToken}
 * authToken parameter is optional.
 **/
$app->get('/api/media/{mediaId}', 'instaGeo.controller:getMediaLocation');

/*
 * PATH: /login
 * Application logout.
 **/
$app->get('/login', 'instaGeo.controller:login');

/*
 * PATH: /logout
 * Application logout.
 **/
$app->get('/logout', 'instaGeo.controller:logout');
