<?php

namespace Exceptions;

use Exception;
use Throwable;

/**
 * Exception class to handle errors in Social Media APIs.
 */
class ServiceApiException extends Exception
{
    /**
     * ServiceApiException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
