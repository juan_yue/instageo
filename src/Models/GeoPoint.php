<?php

namespace Models;

/**
 * Class to define GeoPoint business model.
 **/
class GeoPoint
{
    public $longitude;
    public $latitude;

    /**
     * GeoPoint constructor.
     *
     * @param float $longitude the geopoint longitude
     * @param float $latitude  the geopoint longitude
     */
    public function __construct($longitude, $latitude)
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }
}
