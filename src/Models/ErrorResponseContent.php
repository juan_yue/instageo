<?php

namespace Models;

/**
 * Class to represent HTTP error responses.
 */
class ErrorResponseContent extends ResponseContent
{
    /**
     * ErrorResponseContent constructor.
     *
     * @param int    $code    the error code
     * @param string $message the error message
     */
    public function __construct($code, $message)
    {
        $this->data = null;
        $this->meta = ['code' => $code, 'errorMessage' => $message];
    }
}
