<?php

namespace Models;

/**
 * Class to represent HTTP successful responses.
 */
class OKResponseContent extends ResponseContent
{
    /**
     * OKResponseContent constructor.
     *
     * @param string $data the data to show in the response
     */
    public function __construct($data = '')
    {
        $this->meta = ['code' => 200];
        $this->data = $data;
    }
}
