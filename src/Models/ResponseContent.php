<?php

namespace Models;

/**
 * Class to represent the HTTP API responses.
 **/
abstract class ResponseContent
{
    protected $meta;
    protected $data;

    public function getMeta()
    {
        return $this->meta;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * Returns a JSON representation of the object.
     *
     * @return string
     */
    public function toJSON()
    {
        return json_encode($this->toArray());
    }

    /**
     * Returns an array representation of the object.
     *
     * @return array
     */
    private function toArray()
    {
        return ['meta' => $this->meta, 'data' => $this->data];
    }
}
