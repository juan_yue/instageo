<?php

namespace Models;

/**
 * Class to define Location business model.
 **/
class Location
{
    public $id;
    public $name;
    public $geoPoint;

    /**
     * Location constructor.
     *
     * @param string   $id       the location id
     * @param string   $name     the location name
     * @param GeoPoint $geoPoint the location GeoPoint
     */
    public function __construct($id, $name, $geoPoint)
    {
        $this->id = $id;
        $this->name = $name;
        $this->geoPoint = $geoPoint;
    }
}
