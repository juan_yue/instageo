<?php

namespace Models;

/**
 * Class to define Thumb Images business model.
 **/
class ThumbImage
{
    public $id;
    public $url;

    /**
     * Thumb Image constructor.
     *
     * @param string $id  the thumb image id
     * @param string $url the thumb image url
     */
    public function __construct($id, $url)
    {
        $this->id = $id;
        $this->url = $url;
    }
}
