<?php

namespace Models;

/**
 * Class to define the user details business model.
 */
class UserDetails
{
    public $id;
    public $username;
    public $authToken;

    /**
     * UserDetails constructor.
     *
     * @param string $id        the user id
     * @param string $username  the username
     * @param string $authToken the authorization token
     */
    public function __construct($id, $username, $authToken)
    {
        $this->id = $id;
        $this->username = $username;
        $this->authToken = $authToken;
    }
}
