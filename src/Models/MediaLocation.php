<?php

namespace Models;

/**
 * Class to define Media Location business model.
 **/
class MediaLocation
{
    public $id;
    public $location;

    /**
     * Media Location constructor.
     *
     * @param string $id       the media id
     * @param string $location the media location
     */
    public function __construct($id, $location)
    {
        $this->id = $id;
        $this->location = $location;
    }
}
