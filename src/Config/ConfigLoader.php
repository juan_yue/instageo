<?php

namespace Config;

/**
 * Singleton class to load properties from configurations files.
 */
class ConfigLoader
{
    private static $configLoader;
    private $properties;

    /**
     * ConfigLoader constructor.
     */
    private function __construct()
    {
        $serverName = '';

        if (isset($_SERVER['SERVER_NAME'])) {
            $serverName = $_SERVER['SERVER_NAME'];
        }

        switch ($serverName) {
            case 'localhost':
                $env = '.local';
                break;
            case 'test':
                $env = '.test';
                break;
            default:
                $env = '.local';
                break;
        }

        $this->properties = parse_ini_file(__DIR__.'/../../config/config'.$env.'.ini', true);
    }

    /**
     * Returns an instance of the ConfigLoader and ensures that instance is the only one.
     *
     * @return ConfigLoader
     */
    public static function getInstance()
    {
        if (self::$configLoader == null) {
            self::$configLoader = new self();
        }

        return self::$configLoader;
    }

    /**
     * Returns the API URL of an application.
     *
     * @param string $application the name of the application
     *
     * @return string
     */
    public function getApiUrl($application)
    {
        return $this->properties[$application]['API_URL'];
    }

    /**
     * Returns the Client ID of an application.
     *
     * @param string $application the name of the application
     *
     * @return string
     */
    public function getClientId($application)
    {
        return $this->properties[$application]['CLIENT_ID'];
    }

    /**
     * Returns the Secret Client ID of an application.
     *
     * @param string $application the name of the application
     *
     * @return string
     */
    public function getClientSecretId($application)
    {
        return $this->properties[$application]['CLIENT_SECRET_ID'];
    }

    /**
     * Returns the redirect URL of an application.
     *
     * @param string $application the name of the application
     *
     * @return string
     */
    public function getRedirectUri($application)
    {
        return $this->properties[$application]['REDIRECT_URI'];
    }

    /**
     * Returns the OAuth URL of an application.
     *
     * @param string $application the name of the application
     *
     * @return string
     */
    public function getOauthUrl($application)
    {
        return $this->properties[$application]['OAUTH_URL'];
    }

    /**
     * Returns the API Key of an application.
     *
     * @param string $application the name of the application
     *
     * @return string
     */
    public function getApiKey($application)
    {
        return $this->properties[$application]['API_KEY'];
    }
}
