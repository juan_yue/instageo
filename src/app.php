<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app['twig'] = $app->extend(
    'twig',
    function ($twig) {
        // add custom globals, filters, tags, ...

        return $twig;
    }
);

$app['instaGeo.controller'] = function () use ($app) {
    return new \Controllers\InstaGeoController();
};

require __DIR__.'/../src/Controllers/controllers.php';

return $app;
