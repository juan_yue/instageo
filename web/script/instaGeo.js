function loadMap(latitude, longitude, name) {

    $('#map').empty();

    var myLatLng = {lat: latitude, lng: longitude};

    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 15,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: name
    });
}

function loadLocationName(id, name) {

    var locationName = $('#locationName');
    locationName.empty();
    var link = $('<a href="https://www.instagram.com/explore/locations/' + id + '" target="_blank">' + name + '</a>');
    locationName.append(link);
}

function loadImageGrid(numberImg) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:8000/api/images/" + numberImg,
        success: function (response) {
            $.each(response.data.images, function (i, value) {
                var img = $('<img src=""></img>')
                    .prop('id', value.id)
                    .prop('class', 'igMedia img-polaroid')
                    .prop('src', value.url);
                var node = "#imageGrid\\.img\\[" + (i + 1) + "\\]";
                $(node).append(img);
            });
        }
    });
}

function loadLocation(mediaId) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:8000/api/media/" + mediaId,
        success: function (response) {
            loadLocationName(response.data.location.id, response.data.location.name);
            loadMap(response.data.location.geoPoint.latitude, response.data.location.geoPoint.longitude, response.data.location.name);
        },
        error: function (response) {
            $('#locationName').empty();

            var map = $('#map');
            map.empty();
            var errorMessage = $('<p></p>')
                .prop('class', 'errorMessage')
                .text(response.responseJSON.meta.errorMessage);
            map.append(errorMessage);
        }
    });
}

$(document).ready(function () {
    loadImageGrid(6);

    $('.imageGrid').on("click", ".igMedia", function () {
        loadLocation(this.id);
    });
});