<?php

namespace Services;

use Exceptions\ServiceApiException;
use Models\GeoPoint;
use Models\Location;
use Models\MediaLocation;
use Models\ThumbImage;
use Models\UserDetails;
use PHPUnit\Framework\TestCase;

/**
 * InstaGeoService unit test class.
 */
class InstaGeoServiceTest extends TestCase
{
    const API_NULL_JSON_RESPONSE = '{"meta":{"code":500,"errorMessage":"Social media API cannot be reach"},"data":null}';
    const API_EXCEPTION_MESSAGE = 'Server not found';
    const API_EXCEPTION_JSON_RESPONSE = '{"meta":{"code":404,"errorMessage":"'.self::API_EXCEPTION_MESSAGE.'"},"data":null}';

    private $socialMediaMockAPI;

    public function testGetLoginUrlOk()
    {
        $instagramLoginUrl = 'https://api.instagram.com/oauth';

        $this->socialMediaMockAPI->expects(static::once())
            ->method('getLoginURL')
            ->will(static::returnValue($instagramLoginUrl));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getLoginUrl();

        static::assertEquals($instagramLoginUrl, $result);
    }

    public function testGetLatestThumbImagesOk()
    {
        $thumbImage1 = new ThumbImage(
            '1480017722761907668',
            'https://scontent.cdninstagram.com/t51/.2885-15/s150x150/e35/624_n.jpg'
        );
        $thumbImage2 = new ThumbImage(
            '1480019097336006426',
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/584_n.jpg'
        );

        $this->socialMediaMockAPI->expects(static::once())
            ->method('getRecentMedia')
            ->will(static::returnValue([$thumbImage1, $thumbImage2]));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getLatestThumbImages(2);

        static::assertNotNull($result);
        static::assertEquals(200, $result->getStatusCode());
        static::assertContains('"data":{"total":2,"images":[', $result->getContent());
    }

    public function testGetLatestThumbImagesOkButEmpty()
    {
        $this->socialMediaMockAPI->expects(static::once())
            ->method('getRecentMedia')
            ->will(static::returnValue([]));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getLatestThumbImages(2);

        static::assertNotNull($result);
        static::assertEquals(200, $result->getStatusCode());
        static::assertEquals('{"meta":{"code":200},"data":{"total":0,"images":[]}}', $result->getContent());
    }

    public function testGetLatestThumbImagesFail()
    {
        $this->socialMediaMockAPI->expects(static::once())
            ->method('getRecentMedia')
            ->will(static::throwException(new ServiceApiException(self::API_EXCEPTION_MESSAGE, 404)));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getLatestThumbImages(2, 'aToken');

        static::assertNotNull($result);
        static::assertEquals(404, $result->getStatusCode());
        static::assertEquals(self::API_EXCEPTION_JSON_RESPONSE, $result->getContent());
    }

    public function testGetMediaLocationOk()
    {
        $geopoint = new GeoPoint(-27.051, 27.050);
        $location = new Location('123456789', 'Kame House', $geopoint);
        $mediaLocation = new MediaLocation('mediaId', $location);

        $this->socialMediaMockAPI->expects(static::once())
            ->method('getMedia')
            ->will(static::returnValue($mediaLocation));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getMediaLocation('mediaId');

        static::assertNotNull($result);
        static::assertEquals(200, $result->getStatusCode());

        static::assertNotNull($result->getContent());
        $result = json_decode($result->getContent());

        static::assertEquals('27.050', $result->data->location->geoPoint->latitude);
        static::assertEquals('-27.051', $result->data->location->geoPoint->longitude);
        static::assertEquals('Kame House', $result->data->location->name);
    }

    public function testGetMediaLocationWithoutLocation()
    {
        $mediaLocation = new MediaLocation('mediaIdWithoutLocation', null);

        $this->socialMediaMockAPI->expects(static::once())
            ->method('getMedia')
            ->will(static::returnValue($mediaLocation));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getMediaLocation('mediaIdWithoutLocation');

        static::assertNotNull($result);
        static::assertEquals(404, $result->getStatusCode());
        static::assertContains('Location not found in media', $result->getContent());
    }

    public function testGetMediaLocationFail()
    {
        $this->socialMediaMockAPI->expects(static::once())
            ->method('getMedia')
            ->will(static::throwException(new ServiceApiException(self::API_EXCEPTION_MESSAGE, 404)));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->getMediaLocation('mediaId');

        static::assertNotNull($result);
        static::assertEquals(404, $result->getStatusCode());
        static::assertEquals(self::API_EXCEPTION_JSON_RESPONSE, $result->getContent());
    }

    public function testGenerateTokenOk()
    {
        $userDetails = new UserDetails('1574083', 'goku', 'fb2e77d.47a0479900504cb3ab4a1f626d174d2d');

        $this->socialMediaMockAPI->expects(static::once())
            ->method('generateToken')
            ->will(static::returnValue($userDetails));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->generateToken('code');

        static::assertNotNull($result);
        static::assertEquals(200, $result->getStatusCode());
        static::assertTrue(
            isset($_SESSION['instaGeo_userDetails']) &&
            property_exists($_SESSION['instaGeo_userDetails'], 'authToken')
        );
        static::assertEquals(
            'fb2e77d.47a0479900504cb3ab4a1f626d174d2d',
            $_SESSION['instaGeo_userDetails']->authToken
        );
    }

    public function testGenerateTokenFail()
    {
        $this->socialMediaMockAPI->expects(static::once())
            ->method('generateToken')
            ->will(static::throwException(new ServiceApiException(self::API_EXCEPTION_MESSAGE, 404)));

        $restAPI = new InstaGeoService($this->socialMediaMockAPI);

        $result = $restAPI->generateToken('code');

        static::assertNotNull($result);
        static::assertEquals(404, $result->getStatusCode());
        static::assertEquals(self::API_EXCEPTION_JSON_RESPONSE, $result->getContent());
    }

    protected function setUp()
    {
        $this->socialMediaMockAPI = $this->getMockBuilder('\Services\SocialMediaAPI')
            ->getMock();
    }
}
