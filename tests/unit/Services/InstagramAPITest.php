<?php

namespace Services;

use Config\ConfigLoader;
use Exceptions\ServiceApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

/**
 * InstagramAPI unit test class.
 */
class InstagramAPITest extends TestCase
{
    const JSON_RESPONSE_OK = '{"meta":{"code":200},"data":{}}';

    private static $configLoader;

    public static function setUpBeforeClass()
    {
        $_SERVER['SERVER_NAME'] = 'test';
        self::$configLoader = ConfigLoader::getInstance();
    }

    public function testGetMediaOk()
    {
        $getMediaResponse = '{
           "data":{
              "id":"1234",
              "location":{
                 "latitude":27.050,
                 "longitude":-27.051,
                 "name":"Kame House",
                 "id":123456789
              }
           },
           "meta":{
              "code":200
           }
        }';

        $mock = new MockHandler([new Response(200, [], $getMediaResponse)]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        $mediaLocation = $instagramAPI->getMedia('1234', 'authToken');

        static::assertNotNull($mediaLocation);
        static::assertEquals($mediaLocation->id, '1234');
        static::assertNotNull($mediaLocation->location);
        static::assertEquals($mediaLocation->location->id, '123456789');
        static::assertEquals($mediaLocation->location->name, 'Kame House');
        static::assertNotNull($mediaLocation->location->geoPoint);
        static::assertEquals($mediaLocation->location->geoPoint->latitude, '27.050');
        static::assertEquals($mediaLocation->location->geoPoint->longitude, '-27.051');
    }

    public function testGetMediaOkWithoutLocation()
    {
        $getMediaResponse = '{
           "data":{
              "id":"mediaIdWithoutLocation",
              "location": null
           },
           "meta":{
              "code":200
           }
        }';

        $mock = new MockHandler([new Response(200, [], $getMediaResponse)]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        $mediaLocation = $instagramAPI->getMedia('mediaIdWithoutLocation', 'authToken');

        static::assertNotNull($mediaLocation);
        static::assertEquals($mediaLocation->id, 'mediaIdWithoutLocation');
        static::assertNull($mediaLocation->location);
    }

    public function testGetMediaWithoutToken()
    {
        $instagramAPI = new InstagramAPI($this::$configLoader);

        try {
            $instagramAPI->getMedia('1234', null);
        } catch (ServiceApiException $e) {
            static::assertEquals(403, $e->getCode());
            static::assertEquals('Authentication is required', $e->getMessage());

            return;
        }

        static::fail();
    }

    public function testGetMediaFail()
    {
        $requestException = new RequestException(
            'Media not found',
            new Request('GET', 'test'),
            new Response(404, [], '{"meta":{"code":404,"error_message":"invalid media id"}}')
        );
        $mock = new MockHandler([$requestException]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        try {
            $instagramAPI->getMedia('00000', 'authToken');
        } catch (ServiceApiException $e) {
            static::assertEquals(404, $e->getCode());
            static::assertEquals('invalid media id', $e->getMessage());

            return;
        }

        static::fail();
    }

    public function testGetRecentMediaOk()
    {
        $recentMediaResponse = '{
           "data":[
              {
                 "id":"1482685869323271330",
                 "images":{
                    "thumbnail":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/392_n.jpg"
                    },
                    "low_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/392_n.jpg"
                    },
                    "standard_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/392_n.jpg"
                    }
                 }         
              },
              {
                 "id":"1480019097336006426",
                 "images":{
                    "thumbnail":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/584_n.jpg"
                    },
                    "low_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/584_n.jpg"
                    },
                    "standard_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/584_n.jpg"
                    }
                 }
              },
              {
                 "id":"1480017722761907668",
                 "images":{
                    "thumbnail":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/624_n.jpg"
                    },
                    "low_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/624_n.jpg"
                    },
                    "standard_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/624_n.jpg"
                    }
                 }
              }
           ],
           "meta":{
              "code":200
           }
        }';

        $mock = new MockHandler([new Response(200, [], $recentMediaResponse)]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        $recentMedia = $instagramAPI->getRecentMedia(3, 'authToken');

        static::assertNotNull($recentMedia);
        static::assertEquals(3, count($recentMedia));
        static::assertEquals('1482685869323271330', $recentMedia[0]->id);
        static::assertEquals(
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/392_n.jpg',
            $recentMedia[0]->url
        );
        static::assertEquals('1480019097336006426', $recentMedia[1]->id);
        static::assertEquals(
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/584_n.jpg',
            $recentMedia[1]->url
        );
        static::assertEquals('1480017722761907668', $recentMedia[2]->id);
        static::assertEquals(
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/624_n.jpg',
            $recentMedia[2]->url
        );
    }

    public function testGetLatestThumbImagesMoreThanApi()
    {
        $recentMediaResponse = '{
           "data":[
              {
                 "id":"1482685869323271330",
                 "images":{
                    "thumbnail":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/392_n.jpg"
                    },
                    "low_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/392_n.jpg"
                    },
                    "standard_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/392_n.jpg"
                    }
                 }         
              },
              {
                 "id":"1480019097336006426",
                 "images":{
                    "thumbnail":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/584_n.jpg"
                    },
                    "low_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/584_n.jpg"
                    },
                    "standard_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/584_n.jpg"
                    }
                 }
              },
              {
                 "id":"1480017722761907668",
                 "images":{
                    "thumbnail":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/624_n.jpg"
                    },
                    "low_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/624_n.jpg"
                    },
                    "standard_resolution":{
                       "url":"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/624_n.jpg"
                    }
                 }
              }
           ],
           "meta":{
              "code":200
           }
        }';

        $mock = new MockHandler([new Response(200, [], $recentMediaResponse)]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        $recentMedia = $instagramAPI->getRecentMedia(6, 'authToken');

        //I'm requesting 6 images but Instagram returns 3. Thus, the API will return 3.

        static::assertNotNull($recentMedia);
        static::assertEquals(3, count($recentMedia));
        static::assertEquals('1482685869323271330', $recentMedia[0]->id);
        static::assertEquals(
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/392_n.jpg',
            $recentMedia[0]->url
        );
        static::assertEquals('1480019097336006426', $recentMedia[1]->id);
        static::assertEquals(
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/584_n.jpg',
            $recentMedia[1]->url
        );
        static::assertEquals('1480017722761907668', $recentMedia[2]->id);
        static::assertEquals(
            'https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/624_n.jpg',
            $recentMedia[2]->url
        );
    }

    public function testGetRecentMediaWithoutToken()
    {
        $instagramAPI = new InstagramAPI($this::$configLoader, null);

        try {
            $instagramAPI->getRecentMedia(3, null);
        } catch (ServiceApiException $e) {
            static::assertEquals(403, $e->getCode());
            static::assertEquals('Authentication is required', $e->getMessage());

            return;
        }

        static::fail();
    }

    public function testGetRecentMediaFail()
    {
        $requestException = new RequestException(
            'Internal Server Error',
            new Request('GET', 'test'),
            new Response(500, [], 'Internal Server Error')
        );
        $mock = new MockHandler([$requestException]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        try {
            $instagramAPI->getRecentMedia(3, 'authToken');
        } catch (ServiceApiException $e) {
            static::assertEquals(500, $e->getCode());
            static::assertEquals('Internal Server Error', $e->getMessage());

            return;
        }

        static::fail();
    }

    public function testGetLoginUrl()
    {
        $instagramAPI = new InstagramAPI($this::$configLoader, null);

        static::assertEquals(
            'https://api.instagram.com/oauth/authorize?client_id=7357cl13n7&redirect_uri=http://test:8000/login&response_type=code',
            $instagramAPI->getLoginURL()
        );
    }

    public function testGenerateTokenOk()
    {
        $generateTokenResponse = '{
            "access_token": "fb2e77d.47a0479900504cb3ab4a1f626d174d2d",
            "user": {
                "id": "1574083",
                "username": "goku",
                "full_name": "Son Goku",
                "profile_picture": "..."
            }
        }';

        $mock = new MockHandler([new Response(200, [], $generateTokenResponse)]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        $userDetails = $instagramAPI->generateToken('1234');

        static::assertNotNull($userDetails);
        static::assertEquals('1574083', $userDetails->id);
        static::assertEquals('goku', $userDetails->username);
        static::assertEquals('fb2e77d.47a0479900504cb3ab4a1f626d174d2d', $userDetails->authToken);
    }

    public function testGenerateTokenFail()
    {
        $requestException = new RequestException(
            'Internal Server Error',
            new Request('GET', 'test'),
            new Response(500, [], '{"meta":{"code":500,"error_message":"Internal Server Error"}}')
        );
        $mock = new MockHandler([$requestException]);
        $httpClientMock = new Client(['handler' => HandlerStack::create($mock)]);

        $instagramAPI = new InstagramAPI($this::$configLoader, $httpClientMock);

        try {
            $instagramAPI->generateToken('1234');
        } catch (ServiceApiException $e) {
            static::assertEquals(500, $e->getCode());
            static::assertEquals('Internal Server Error', $e->getMessage());

            return;
        }

        static::fail();
    }
}
